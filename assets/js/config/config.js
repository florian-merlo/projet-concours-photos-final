var configGlobale = {
    base_url: 'http://localhost/ConcoursPhotos/',
    host_mqtt : 'localhost',
    port_ws : 9001,
    enabled: true  
  };

var configMQTT = {
    useSSL: false,
    userName: "userPhotos",
    password: "passPhotos",
    onSuccess: onConnect,
    onFailure: doFail
}