$(document).ready(function(){
    var nomcompete;
    var idcompete;
//--------lors de l'appuie sur un des boutons de choix de compete----------------------------------------------
    $('.BtnCompet').click(function(e){

        e.preventDefault();
         idcompete = $(this).data("id");
        
        if (idcompete==1){
          nomcompete='Couleur';
        }
         if(idcompete == 2){
            nomcompete='Monochrome';
        }

        if (idcompete==3){
             nomcompete='Nature';
        }

        $('#compet').after('<input type="submit" id="Aleatoire" data-id="btnAlea" value="Générez un ordre de projection" />');
        $('#compet').after("<h1>Génerez l'ordre de passage avec ce bouton</h1>");
        $('#compet').after("<br><p>Competition choisie: "+nomcompete+"</p>");
        //document.getElementById('.BtnCompet').disabled = true;

        $('.BtnCompet').attr('disabled', 'disabled');

    });

//----------Clique sur le bouton pour générer l'ordre aléatoire de passage------------------------------------------------
    $('#Aleatoire').click(function(e){
        e.preventDefault();

        var url = "http://192.168.0.42/ConcoursPhotos/REST/Photo/competition/"+idcompete;
        $.ajax({
            type: "GET",
            url: url,
            dataType: "json",
            json: "json",
            success: onGetSuccess,
            error: onGetError   
        });
    });   

function onGetSuccess(reponse, status) {

    
}

function onGetError(status) {
    alert('Un problème est survenue');
}
});
