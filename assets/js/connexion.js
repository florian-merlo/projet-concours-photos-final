if(configGlobale.enabled = true){
    var base_url = configGlobale.base_url;
}


$(document).ready(function(){
    //au démarage de la page
    
    $("#formConnexion").submit(function(e){

        e.preventDefault();
        var postdata = $("#formConnexion").serialize();


    $.ajax({
        type: "POST",
        url: base_url + "REST/Photo/connexion/",
        dataType: "json",
        json: "json",
        data: postdata,
        success: onGetSuccess,
        error: onGetError   
    });

    function onGetSuccess(reponse, status) { 

         if(reponse.isSuccess){

            console.log(reponse) ; 
        
            window.location.href = base_url + "C_Connexion/";

         }else{
             
            $("#login").val('');
            $("#pass").val('');
            $("#labelerror").text('Saisie incorrecte');

            console.log(reponse) ; 

         }
    }
    
    function onGetError(status) {

        console.log('Server Injoinable');
        
    }
});

});