
<!DOCTYPE HTML>
<html>

    <head>
        <title><?php echo $titre; ?></title>
        <meta name="description" content="website description" />
        <meta name="keywords" content="website keywords, website keywords" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />      
        <link rel="icon" href="<?php echo base_url('assets/img/icohead.jpg'); ?>" />   
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" title="style" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/competition.css'); ?>" title="style" />
        <script type="text/javascript" src="<?php echo base_url('assets/js/libs/jquery.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/libs/mqttws31.min.js'); ?>"></script>  
        <script type="text/javascript" src="<?php echo base_url('assets/js/config/config.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/js/mqtt.js'); ?>"></script>
    </head>

    <body>
        <h1 id="titleComp">Compétition Nature</h1>

        <div id="divImgPts">
            <img src="<?php echo base_url('assets/img/phototestnature.jpg'); ?>" id="imgComp">

            <h2 id="ptsComp">--</h2>
        </div>
        <br>
        <div id="divJuryComp"></div>
        <footer id="footer">
            <div id="divPSP">
                <input type="image" id="imgPrec" class="imgsPSP" width="100" height="100" 
                       src="<?php echo base_url("assets/img/iconprecedent.png"); ?>">


                <input type="image" id="imgPause" class="imgsPSP" width="100" height="100" onclick="onBtnPause();" 
                       src="<?php echo base_url("assets/img/iconpause.png"); ?>">


                <input type="image" id="imgSuiv" class="imgsPSP" width="100" height="100" onclick="onBtnSuiv();" 
                       src="<?php echo base_url("assets/img/iconsuivant.png"); ?>">
            </div>
            <div id="divBtnClass"></div>
        </footer>
    </body>
</html>

