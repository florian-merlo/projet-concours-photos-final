<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="icon" href="<?php echo base_url('assets/img/icohead.jpg'); ?>" />
    <title>Connexion | Concours Photos</title>
    <link href="<?php echo base_url('assets/css/connexion.css'); ?>" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/config/config.js'); ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/connexion.js'); ?>"></script>

</head>

<body>

    <form action="<?= $_SERVER['PHP_SELF']; ?>" method="POST" id="formConnexion">
        <div class="container">
            <div id="titleConnexion">
                <label>Connexion</label><br><br>
            </div>

            <label for="uname"><b>Login
                </b></label>
            <br>
            <input type="text" placeholder="Entrez votre Login" name="login" id="login" required>
            <br><br><br>
            <label for="psw"><b>Mot de passe</b></label>
            <input type="password" placeholder="Entrez votre mot de passe" name="pass" id="pass" required>
            <br><br><br>
            <button type="submit">Login</button>
            <div style="color: red;">
                <br>
                <label id="labelerror"></label>
            </div>
        </div>
    </form>

</body>

</html>

<style>

</style>