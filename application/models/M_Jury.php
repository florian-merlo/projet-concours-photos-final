<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Jury extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();    //connexion bdd
    }
//Ajouter un jury------------------------------------------------------------------------------
    public function ajouter_jury($nomJury, $prenomJury) {
        if(($nomJury != '') && ($prenomJury != '')){
        $data = array(
            'Nom' => $nomJury,
            'Prenom' => $prenomJury
    );
        $this->db->insert('jury', $data);

        $last_id = $this->db->insert_id() ;
        return $this->lire_jury_by_id($last_id) ;
    }
    }
//Lire un jury-----------------------------------------------------------------------------------
    public function lire_jury(){
        $query= $this->db->select('*')
                         ->from ('jury')
                         ->get();

        return $query->result_array();
    }
   
    public function lire_jury_by_id($id){
        $query= $this->db->select('*')
                         ->from ('jury')
                         ->where('ID', $id)
                         ->get();

        return $query->result_array();
    }

}

