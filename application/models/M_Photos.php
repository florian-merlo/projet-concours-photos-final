<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Photos extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_photos_by_comp($idcomp) {

        $query = $this->db->select("*")
                ->from('photo')
                ->where('competitionID', $idcomp)
                ->order_by('OrdreProjection')
                ->get();
        return $query->result_array();
       
    }

}

