<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Connexion extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function get_user($login, $pass) {

        $result = null;

        $query = $this->db->select('*')
            ->from('organisateur')
            ->where('login', $login)
            ->where('password', $pass)
            ->get();
        $array = $query->result_array();

        if(count($query) == 1)
        {
            $result = $array ; 
        }

        return $result ;

    }
    /* public function add_user($login, $pass) {
        $data = array(
            'nom' => 'Merlo',
            'prenom' => 'Florian',
            'login' => $login,
            'password' => $pass
    );
        $query = $this->db->insert('organisateur', $data);



    }*/

}

