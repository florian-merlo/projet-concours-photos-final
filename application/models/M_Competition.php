<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class M_Competition extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

    public function search_compet($search) {
        $query = $this->db->select('ID')
                            ->from('competition')
                            ->where('Nom', $search)
                            ->get();
        
        return $query->result_array();
       
    }
    

    public function lire_donnee_compete($idcompet){
        $query = $this->db->select('ID', 'OrdrePrjection')
                            ->from('photo')
                            ->where('competition',$idcompet)
                            ->get();
        return $query->result_array();
    }
}
