<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_Competition extends CI_Controller {

    public function __construct() {
        parent::__construct();
        session_start();

    }

    public function index() {
        if (!isset($_SESSION['user'])) {

            header("Location: " . base_url("C_Connexion"));


        }else{
            $data['titre'] = "Compétition | Concours Photos";
            $data['titrepage'] = "Choix de la compétition";
            $page = $this->load->view('V_NewCompetition', $data, true);
            $this->load->view('template/V_Template', array('contenu' => $page));

        }

    }


    public function ordre_alea(){


        $data['titre'] = "Compétition | Concours Photos";
        $data['titrepage'] = "Choix de la compétition";
        $data['titreChoix'] = "Génerez l'ordre de passage avec ce bouton";
        $data['bouton'] = '<input type="submit" id="btnAlea" value="Générez un ordre de projection" />';
        $data['commencer']= '<input type="submit" id="btnSuite" value="Commencer la compétition"/>';

        $page = $this->load->view('V_NewCompetition', $data, true);
        $this->load->view('template/V_Template', array('contenu' => $page));
    }

    public function nouvelle_competition(){
        if (!isset($_SESSION['user'])) {

            header("Location: " . base_url("C_Connexion"));


        }else{

            $data['titre'] = "Compétition | Concours Photos";

            $this->load->view('V_Competition', $data);

        }

    }

}