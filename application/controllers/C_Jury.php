<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_Jury extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('M_Jury'); 
        session_start();
    }

    public function index() {

        if (!isset($_SESSION['user'])) {

            header("Location: " . base_url("C_Connexion"));


        }else{
            //afficher la page
            //$res = $this->M_jury->lire_jury();

            $data['titre'] = "Compétition | Concours Photos";
            $data['titrepage'] = "Liste des jury enregistrés pour la notation des photos";

            //$data['resultat']= $res;
            $page = $this->load->view('V_Jury', $data, true);
            $this->load->view('template/V_Template', array('contenu' => $page));

        }

    }

    /*public function ajout(){
        //recuperer les infos dans les zones de textes
       // $nom = $this->input->post('txtNomJury');
       // $prenom = $this->input->post('txtPrenomJury');


        //afficher la page


       // $data['tab'] = $res;
        $data['titre'] = "Compétition | Concours Photos";
        $data['titrepage'] = "Liste des jury enregistrés pour la notation des photos";
        $page = $this->load->view('V_Jury', $data, true);
        $this->load->view('template/V_Template', array('contenu' => $page));  
    }*/
}