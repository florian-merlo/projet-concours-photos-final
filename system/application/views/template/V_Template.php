
<!DOCTYPE HTML>
<html>

<head>
  <title><?php echo $titre; ?></title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/photos.css'); ?>" title="style" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/paho-mqtt/1.0.1/mqttws31.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo base_url('assets/js/mqtt.js'); ?>"></script>
</head>

<body>
  <div id="main">
    <div id="header">
      <a id="btnDeco" href="<?php echo site_url('C_Connexion/logout'); ?>">Déconnexion</a>
      <div id="logo">
        <div id="logo_text">
  
          <h1><a href="<?php echo site_url('C_Connexion/return_accueil'); ?>">Concours photos</a></h1>
          <h2>Connecté en tant que: <?php ucfirst($_SESSION['user']); ?></h2>
          
        </div>
         <img id="img" src="<?php echo base_url('assets/img/icohead.jpg'); ?>" alt=""/>
      </div>
      <div id="menubar">
        <ul id="menu">
         
          <li class="selected"><a href="<?php echo site_url('C_Connexion/return_accueil'); ?>">Accueil</a></li>
          <li><a href="<?php echo site_url('C_Jury'); ?>">Jury</a></li>
          <li><a href="<?php echo site_url('C_Competition'); ?>">Nouvelle competition</a></li>
          <!--<li><a href="<?php //echo site_url('C_Photos/afficher_photos/1'); ?>">Photos</a></li>-->
          <li><a href="#">Importation base</a></li>
          
        </ul>
      </div>
    </div>
    <div id="site_content">
      <?php echo $contenu; ?>
    </div>
    <div id="footer">
      Copyright &copy; BTS SNIR Armentières
    </div>
  </div>
</body>


</html>


