<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link rel="icon" href="<?php echo base_url('assets/img/icohead.jpg'); ?>" />
        <title>Connexion | Concours Photos</title>
        <link href="<?php echo base_url('assets/css/connexion.css'); ?>" rel="stylesheet" type="text/css">

    </head>
    <body>

        <form action="<?php echo site_url('C_Connexion/process'); ?>" method="post">
            <div class="container">
                <div id="titleConnexion">
                <label>Connexion</label><br><br>
                </div>
                
                <label for="uname"><b>Login
                </b></label>
                <br>
                <input type="text" placeholder="Entrez votre Login" name="login" required>
<br><br><br>
                <label for="psw"><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrez votre mot de passe" name="pass" required>
                <br><br><br>
                <button type="submit">Login</button>
                <div style="color: red;">
            <br>
            <?php
            if (isset($erreur)) {
                echo $erreur;
            }
            ?>
        </div>
            </div>
        </form>
        
    </body>
</html>

<style>

</style>

