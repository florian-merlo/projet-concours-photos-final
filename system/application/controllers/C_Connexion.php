<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_Connexion extends CI_Controller {

    public function __construct() {
        parent::__construct();
          $this->load->model('M_Connexion');
         $this->load->model('M_Photos');
           $this->load->driver('session');
    }

    public function index() {
        $this->load->view('V_Connexion');
        /*$pass = "Aqwxsz!";
        $pass = md5($pass);
        $pass = sha1($pass);
        $pass = md5($pass);
        $this->M_Connexion->add_user('administrateur', $pass);*/
    }

    public function process() {
      
        $login = $this->input->post('login');
        $pass = $this->input->post('pass');
        $pass = md5($pass);
        $pass = sha1($pass);
        $pass = md5($pass);
        $array_resultat = $this->M_Connexion->get_user($login, $pass);
        if (count($array_resultat) > 0) {
            //declaring session 
                      
            $this->session->set_userdata(array('user' => $array_resultat[0]['login']));
            $this->session->set_userdata(array('nom' => $array_resultat[0]['nom']));
            $this->session->set_userdata(array('prenom' => $array_resultat[0]['prenom']));
           
            $data['titre'] = "Accueil | Concours Photos";
            $page = $this->load->view('V_Accueil', $data, true);
        $this->load->view('template/V_Template', array('contenu' => $page));
        } else {
            $data['erreur'] = 'Votre compte n\'existe pas';
            $this->load->view('V_Connexion', $data);
        }
       
    }

    public function logout() {
        //removing session  
        $this->session->unset_userdata(array('user', 'nom', 'prenom'));
        session_destroy;
        redirect("C_Connexion");
    }

    public function afficher_photos($competition) {
        $array_resultat = $this->M_Photos->get_photos($competition);
        return $array_resultat;
     }

     public function return_accueil() {
        $data['titre'] = "Accueil | Concours Photos";
        $page = $this->load->view('V_Accueil', $data, true);
    $this->load->view('template/V_Template', array('contenu' => $page));
     }

}
