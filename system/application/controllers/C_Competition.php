<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class C_Competition extends CI_Controller {

    public function __construct() {
        parent::__construct();
          
    }

    public function index() {
        $data['titre'] = "Compétition | Concours Photos";
    
        $page = $this->load->view('V_Competition', $data);
    }
    
    

}